package types

type InputGetList struct {
	Page     int    `json:"page"`
	PageSize int    `json:"page_size"`
	OrderBy  string `json:"order_by"`
	OrderDir string `json:"order_direction" example:"asc|desc"`
	Search   string `json:"search"`
}
