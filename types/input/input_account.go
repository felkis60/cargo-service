package types

type InputCreateEditAccount struct {
	Name  *string `json:"name"`
	Token *string `json:"token"`
}
