package types

type OperationFilters struct {
}

type InputGetListOperation struct {
	Page     int                `json:"page"`
	PageSize int                `json:"page_size"`
	OrderBy  string             `json:"order_by"`
	OrderDir string             `json:"order_direction" example:"asc|desc"`
	Search   string             `json:"search"`
	Filters  ProductListFilters `json:"filters"`
}

type InputCreateEditOperation struct {
	ProductsUID *string `json:"products_uid"`
	Amount      *int64  `json:"amount"`
	FromData    *string `json:"from_data"`
	ToData      *string `json:"to_data"`
}
