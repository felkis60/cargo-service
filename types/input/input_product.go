package types

type ProductListFilters struct {
}

type InputGetListProduct struct {
	Page     int                `json:"page"`
	PageSize int                `json:"page_size"`
	OrderBy  string             `json:"order_by"`
	OrderDir string             `json:"order_direction" example:"asc|desc"`
	Search   string             `json:"search"`
	Filters  ProductListFilters `json:"filters"`
}

type InputCreateEditProduct struct {
	Name         *string `json:"name"`
	Description  *string `json:"description"`
	CustomFields *string `json:"custom_fields"`
}
