package types

import (
	"time"

	"gorm.io/gorm"
)

type Operation struct {
	ID         int64  `json:"id" gorm:"primarykey"`
	UID        string `json:"uid"`
	AccountsID int64  `json:"accounts_id"`
	ProductsID int64  `json:"products_id"`
	Amount     int64  `json:"amount"`
	FromData   string `json:"from_data"`
	ToData     string `json:"to_data"`

	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}

//Номер заявки, тип заявки(забор,доставка), адрес доставки, окно доставки, список позиций(с полным объемом данных), authUsersUID(Курьер),  authUsersUID(тот кто создал заявку),общий вес, общий объем, статус

//Заборы и доставка объединяются в один заказ
//Манагер сам пишет сколько и откуда
//Может добавить забор

//У одной доставки может быть несколько заборов, но у забора может быть толлько одна доставка.
//Заказ(забор, доставка)
//Коментарий,список груза, имя, телефон, окно, цена
