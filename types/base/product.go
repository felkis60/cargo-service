package types

import (
	"time"

	"gorm.io/gorm"
)

type Product struct {
	ID           int64  `json:"id" gorm:"primarykey"`
	UID          string `json:"uid"`
	AccountsID   int64  `json:"accounts_id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	CustomFields string `json:"custom_fields"`

	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}
