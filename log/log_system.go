package log

import (
	"log"
	"os"
)

func StartLogs() {

	file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	log.SetOutput(file)
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	if os.Getenv("INFOLOG") == "true" {
		PrintInfo = true
	}
}

const (
	ERR                   = "ERROR: %v"
	ERRWrongData          = "ERROR: Wrong data"
	ERREnvLoad            = "ERROR: Failed to load .env file"
	ERRDbConnect          = "ERROR: Failed to connect database: %v"
	ERRAutoMigrate        = "ERROR: Failed to automigrate: %v"
	ERRListen             = "ERROR: Failed to listen: %v"
	ERRServe              = "ERROR: Failed to serve: %v"
	ERRTokenReq           = "ERROR: Token is required"
	ERRUIDReq             = "ERROR: 'uid' is required"
	ERRPhoneReq           = "ERROR: 'phone' is required"
	ERROrderUIDReq        = "ERROR: 'order_uid' is required"
	ErrNameReq            = "ERROR: 'name' is required"
	ErrCodeReq            = "ERROR: 'code' is required"
	ErrAmountReq          = "ERROR: 'amount' is required"
	ErrDateReq            = "ERROR: 'date' is required"
	ERRNoSuchToken        = "ERROR: No such Token"
	ERRNoSuchOrder        = "ERROR: No such order"
	ERRNoSuchCategory     = "ERROR: No such category"
	ERRNoSuchUID          = "ERROR: No such 'uid'"
	ERRTokenIsOccupied    = "ERROR: Token is occupied"
	ERRSameUID            = "ERROR: Same UID, need another"
	ERRNoSuchProductUID   = "ERROR: No such Product 'uid'"
	ERRNoSuchProductID    = "ERROR: No such Product 'id'"
	ERRNoSuchOperationUID = "ERROR: No such Operation 'uid'"
	ERRNoSuchOperationID  = "ERROR: No such Operation 'id'"
	ERRNoSuchID           = "ERROR: No such 'id'"
	ERRNegativeAmount     = "ERROR: Products amount can't be negative"
	ERREmptyAmount        = "ERROR: Products amount can't be zero"

	INF            = "INFO: %v"
	INFReceived    = "INFO: Received: %v"
	INFStartServer = "INFO: %v server starts"
	INFEndServer   = "INFO: %v server ends"
	INFFuncStart   = "INFO: Function %v start"
	INFFuncEnd     = "INFO: Function %v end"
)

var (
	PrintInfo = false
)
