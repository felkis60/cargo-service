package repository

import (
	"errors"

	logs "goods-service/log"
	bt "goods-service/types/base"
	it "goods-service/types/input"

	"gorm.io/gorm"
)

type AccountRepository struct {
	Db *gorm.DB
}

func writeAccount(account *bt.Account, input *it.InputCreateEditAccount) error {
	if input.Name != nil {
		account.Name = *input.Name
	}
	if input.Token != nil {
		account.Token = *input.Token
	}
	if account.Name == "" {
		return errors.New(logs.ErrNameReq)
	}
	if account.Token == "" {
		return errors.New(logs.ERRTokenReq)
	}
	return nil
}

func (rep *AccountRepository) Create(data *bt.Account, input *it.InputCreateEditAccount) error {

	if err := writeAccount(data, input); err != nil {
		return err
	}

	var account bt.Account
	if result := rep.Db.Find(&account, "token = ?", data.Token); result.RowsAffected != 0 {
		return errors.New(logs.ERRTokenIsOccupied)
	}

	rep.Db.Create(data)

	return nil
}

func (rep *AccountRepository) Edit(data *bt.Account, input *it.InputCreateEditAccount, token string) error {

	if input.Token != nil && *input.Token != data.Token {
		var account bt.Account
		if result := rep.Db.Find(&account, "token = ?", input.Token); result.RowsAffected != 0 {
			return errors.New(logs.ERRTokenIsOccupied)
		}
	}

	writeAccount(data, input)

	rep.Db.Save(data)
	return nil
}

func (rep *AccountRepository) Get(token string) (*bt.Account, error) {
	var data bt.Account
	if result := rep.Db.Find(&data, "token = ?", token); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchToken)
	}
	return &data, nil
}
