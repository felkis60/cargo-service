package repository

import (
	"errors"
	logs "goods-service/log"
	bt "goods-service/types/base"
	types "goods-service/types/base"
	it "goods-service/types/input"
	"math"

	"github.com/gofrs/uuid"
	helpers "gitlab.com/felkis60/rex-microservices-helpers"
	"gorm.io/gorm"
)

const (
	operationTableName = "operations"
)

type OperationRepository struct {
	Db *gorm.DB
}

func (rep *OperationRepository) writeOperation(data *bt.Operation, input *it.InputCreateEditOperation) error {
	var product types.Product
	if input.Amount != nil {
		data.Amount = *input.Amount
	}
	if input.FromData != nil {
		data.FromData = *input.FromData
	}
	if input.ToData != nil {
		data.ToData = *input.ToData
	}
	if data.Amount == 0 {
		return errors.New(logs.ERREmptyAmount)
	}

	if result := rep.Db.Where("uid = ?", input.ProductsUID).Where("deleted_at IS NULL AND accounts_id = ?", data.AccountsID).Limit(1).Find(&product); result.Error != nil {
		return result.Error
	}
	data.ProductsID = product.ID

	//добавить как метод perationRepository
	//поиск продукта по аккаунту (посмтреть в кэшфлоу)
	//Добавить проверку и добавление Продукта по его UID

	return nil
}

func (rep *OperationRepository) Create(input *it.InputCreateEditOperation, accountID int64) (*bt.Operation, error) {

	var (
		data bt.Operation
		totalAmount int64
	)

	data.AccountsID = accountID
	data.UID = uuid.Must(uuid.NewV4()).String()
	if err := rep.writeOperation(&data, input); err != nil {
		return nil, err
	}

	// добавить привязку к аккаунту , сделать функцию суммирования (с кэшем)
	if data.Amount < 0 {
		if err := rep.SummAmount(data.ProductsID, data.AccountsID, &totalAmount); err != nil {
			return nil, err
		}

		if totalAmount+data.Amount < 0 {
			return nil, errors.New(logs.ERRNegativeAmount)
		}
	}

	if result := rep.Db.Create(&data); result.Error != nil {
		return nil, result.Error
	}

	return &data, nil
}

func (rep *OperationRepository) Edit(UID string, accountToken string, input *it.InputCreateEditOperation) (*bt.Operation, error) {
	var (
		totalAmount    int64
		previousAmount int64
	)
	data, err := rep.GetByUID(UID, accountToken)
	if err != nil {
		return nil, err
	}
	previousAmount = data.Amount

	if err := rep.writeOperation(data, input); err != nil {
		return data, err
	}

	if data.Amount < previousAmount {
		if err := rep.SummAmount(data.ProductsID, data.AccountsID, &totalAmount); err != nil {
			return nil, err
		}
		if totalAmount-previousAmount+data.Amount < 0 {
			return nil, errors.New(logs.ERRNegativeAmount)
		}
	}

	if result := rep.Db.Save(data); result.Error != nil {
		return nil, result.Error
	}

	return data, nil
}

func (rep *OperationRepository) Delete(UID string, accountToken string) error {
	var (
		totalAmount int64
	)
	data, err := rep.GetByUID(UID, accountToken)
	if err != nil {
		return err
	}

	if data.Amount > 0 {
		if err := rep.SummAmount(data.ProductsID, data.AccountsID, &totalAmount); err != nil {
			return err
		}
		if totalAmount-data.Amount < 0 {
			return errors.New(logs.ERRNegativeAmount)
		}
	}

	if result := rep.Db.Delete(data); result.Error != nil {
		return result.Error
	}

	return nil
}

func (rep *OperationRepository) List(accountToken string, input *it.InputGetListOperation) (*helpers.Pagination, error) {

	var tx = helpers.DbAccountLeftJoin(operationTableName, accountToken, rep.Db.Model(&bt.Operation{}))

	//Filters
	if input.Search != "" {
		//operationString := "%" + input.Search + "%"
		//tx = tx.Where("("+operationTableName+".name ILIKE ?)", operationString, operationString)
	}

	//New session
	tx = tx.Session(&gorm.Session{})

	var totalItems int64
	tx.Count(&totalItems)

	var orderString = ""
	if input.OrderBy != "" {
		orderString = operationTableName + "." + input.OrderBy
	}

	if input.OrderDir != "" {
		orderString += " " + input.OrderDir
	}

	if orderString != "" {
		tx = tx.Order(orderString)
	}

	var data []types.Operation
	if result := helpers.PaginateManual(&input.Page, &input.PageSize, 5, 1000)(tx).Scan(&data); result.Error != nil {
		return nil, result.Error
	}

	var totalPages = int(math.Ceil(float64(totalItems) / float64(input.PageSize)))

	return &helpers.Pagination{Page: &input.Page, Items: data, TotalItems: &totalItems, TotalPages: &totalPages}, nil

}

func (rep *OperationRepository) GetByID(ID int64, accountToken string) (*bt.Operation, error) {
	var data bt.Operation
	if result := helpers.DbAccountLeftJoin(operationTableName, accountToken, rep.Db).Find(&data, operationTableName+".id = ?", ID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchOperationID)
	}
	return &data, nil
}

func (rep *OperationRepository) GetByUID(UID string, accountToken string) (*bt.Operation, error) {
	var data bt.Operation
	if result := helpers.DbAccountLeftJoin(operationTableName, accountToken, rep.Db).Find(&data, operationTableName+".uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchOperationUID)
	}
	return &data, nil
}

func (rep *OperationRepository) SummAmount(ProductsID int64, AccountsID int64, totalAmount *int64) (error) {
	if result := rep.Db.Raw("SELECT SUM(operations.amount) AS total FROM operations WHERE operations.deleted_at IS NULL AND operations.products_id = ? AND operations.accounts_id = ?", ProductsID, AccountsID).Scan(totalAmount); result.Error != nil {
		return result.Error
	}
	return nil
}
