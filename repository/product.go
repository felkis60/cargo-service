package repository

import (
	"errors"
	"math"

	logs "goods-service/log"

	bt "goods-service/types/base"
	types "goods-service/types/base"
	it "goods-service/types/input"

	"github.com/gofrs/uuid"
	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"gorm.io/gorm"
)

const (
	productTableName = "products"
)

type ProductRepository struct {
	Db *gorm.DB
}

func writeProduct(data *bt.Product, input *it.InputCreateEditProduct) error {
	if input.Name != nil {
		data.Name = *input.Name
	}
	if input.Description != nil {
		data.Description = *input.Description
	}
	if input.CustomFields != nil {
		data.CustomFields = *input.CustomFields
	}

	return nil
}

func (rep *ProductRepository) Create(input *it.InputCreateEditProduct, accountID int64) (*bt.Product, error) {

	var data bt.Product

	if err := writeProduct(&data, input); err != nil {
		return nil, err
	}

	data.AccountsID = accountID
	data.UID = uuid.Must(uuid.NewV4()).String()

	if result := rep.Db.Create(&data); result.Error != nil {
		return nil, result.Error
	}

	return &data, nil
}

func (rep *ProductRepository) Edit(UID string, accountToken string, input *it.InputCreateEditProduct) (*bt.Product, error) {

	data, err := rep.GetByUID(UID, accountToken)
	if err != nil {
		return nil, err
	}

	if err := writeProduct(data, input); err != nil {
		return data, err
	}

	if result := rep.Db.Save(data); result.Error != nil {
		return nil, result.Error
	}

	return data, nil
}

func (rep *ProductRepository) Delete(UID string, accountToken string) error {

	data, err := rep.GetByUID(UID, accountToken)
	if err != nil {
		return err
	}

	if result := rep.Db.Delete(data); result.Error != nil {
		return result.Error
	}

	return nil
}

func (rep *ProductRepository) List(accountToken string, input *it.InputGetListProduct) (*helpers.Pagination, error) {

	var tx = helpers.DbAccountLeftJoin(productTableName, accountToken, rep.Db.Model(&bt.Product{}))

	//Filters
	if input.Search != "" {
		//productString := "%" + input.Search + "%"
		//tx = tx.Where("("+productTableName+".name ILIKE ?)", productString, productString)
	}

	//New session
	tx = tx.Session(&gorm.Session{})

	var totalItems int64
	tx.Count(&totalItems)

	var orderString = ""
	if input.OrderBy != "" {
		orderString = productTableName + "." + input.OrderBy
	}

	if input.OrderDir != "" {
		orderString += " " + input.OrderDir
	}

	if orderString != "" {
		tx = tx.Order(orderString)
	}

	var data []types.Product
	if result := helpers.PaginateManual(&input.Page, &input.PageSize, 5, 1000)(tx).Scan(&data); result.Error != nil {
		return nil, result.Error
	}

	var totalPages = int(math.Ceil(float64(totalItems) / float64(input.PageSize)))

	return &helpers.Pagination{Page: &input.Page, Items: data, TotalItems: &totalItems, TotalPages: &totalPages}, nil

}

func (rep *ProductRepository) GetByID(ID int64, accountToken string) (*bt.Product, error) {
	var data bt.Product
	if result := helpers.DbAccountLeftJoin(productTableName, accountToken, rep.Db).Find(&data, productTableName+".id = ?", ID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchProductID)
	}
	return &data, nil
}

func (rep *ProductRepository) GetByUID(UID string, accountToken string) (*bt.Product, error) {
	var data bt.Product
	if result := helpers.DbAccountLeftJoin(productTableName, accountToken, rep.Db).Find(&data, productTableName+".uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchProductUID)
	}
	return &data, nil
}
