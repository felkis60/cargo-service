package main

import (
	"os"
	"testing"

	setup "goods-service/api/rest/router"
	db "goods-service/database"
	start "goods-service/init"
	bt "goods-service/types/base"

	"github.com/gin-gonic/gin"
)

var router *gin.Engine

const rightToken = "123"
const rightToken2 = "1234"

func TestMain(m *testing.M) {

	os.Setenv("HOST", "localhost")
	os.Setenv("DB_USER", "postgres")
	os.Setenv("DB_PASSWORD", "admin")
	os.Setenv("DB_NAME", "product_test")
	os.Setenv("DB_PORT", "5432")
	os.Setenv("DB_SSL_MODE", "disable")
	os.Setenv("DB_TIMEZONE", "Europe/Moscow")
	os.Setenv("INFO_LOG", "false")
	os.Setenv("REDIS_PORT", "6379")

	defer os.Clearenv()
	start.SystemStartup(false, true)
	router = setup.SetupRouter()

	var result int
	db.Db.Raw("DELETE FROM accounts").Scan(&result)
	db.Db.Raw("DELETE FROM products").Scan(&result)
	db.Db.Raw("DELETE FROM operations").Scan(&result)

	db.Db.Create(&bt.Account{Name: "test", Token: rightToken})
	db.Db.Create(&bt.Account{Name: "test2", Token: rightToken2})

	code := m.Run()

	os.Exit(code)
}
