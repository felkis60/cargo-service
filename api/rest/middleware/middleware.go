package middleware

import (
	db "goods-service/database"
	logs "goods-service/log"
	bt "goods-service/types/base"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

func TokenLogin() gin.HandlerFunc {
	return func(c *gin.Context) {
		var token helpers.TokenHeader
		if err := c.ShouldBindHeader(&token); err != nil {
			c.AbortWithStatusJSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
			return
		}

		if token.Token == "" {
			c.AbortWithStatusJSON(400, helpers.RestRespone{Message: logs.ERRTokenReq, Payload: nil})
			return
		}

		var account bt.Account
		if result := db.Db.Find(&account, "token = ?", token.Token); result.RowsAffected == 0 {
			c.AbortWithStatusJSON(400, helpers.RestRespone{Message: logs.ERRNoSuchToken, Payload: nil})
			return
		}

		c.Set("token", token.Token)
		c.Set("account_id", account.ID)

		c.Next()
	}
}

func UIDHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		uid := c.Param("uid")

		if uid == "" {
			c.AbortWithStatusJSON(400, helpers.RestRespone{Message: logs.ERRNoSuchUID, Payload: nil})
		}

		c.Set("uid", uid)

		c.Next()

	}
}

func IDHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		id := c.Param("id")

		if id == "" {
			c.AbortWithStatusJSON(400, gin.H{"error": logs.ERRNoSuchID})
		}

		c.Set("id", id)

		c.Next()

	}
}
