package other

import (
	controller "goods-service/api/rest/controllers"
	middleware "goods-service/api/rest/middleware"
	db "goods-service/database"
	repos "goods-service/repository"

	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()

	v1 := r.Group("/v1")
	{
		{
			accounts := v1.Group("/accounts")
			rep := repos.AccountRepository{Db: db.Db}
			ctrl := &controller.AccountController{Repository: rep}

			accounts.POST("", ctrl.Create)
			accounts.POST("/", ctrl.Create)
			accounts.POST("/:token", ctrl.Get)
			accounts.POST("/:token/edit", ctrl.Edit)
		}

		{
			products := v1.Group("/products").Use(middleware.TokenLogin())
			rep := repos.ProductRepository{Db: db.Db}
			ctrl := &controller.ProductController{Repository: rep}

			products.POST("", ctrl.Create)
			products.POST("/", ctrl.Create)
			products.POST("/list", ctrl.List)
			products.POST("/edit", ctrl.Edit)

			{
				//productsID := products.Use(middleware.IDHandler())

				//productsID.POST("/:id", ctrl.GetByID)
			}

			{
				productsUID := products.Use(middleware.UIDHandler())

				productsUID.POST("/:uid", ctrl.GetByUID)
				productsUID.POST("/:uid/edit", ctrl.Edit)
				productsUID.POST("/:uid/delete", ctrl.Delete)
			}

		}

		{
			group := v1.Group("/operations").Use(middleware.TokenLogin())
			rep := repos.OperationRepository{Db: db.Db}
			ctrl := &controller.OperationController{Repository: rep}

			group.POST("", ctrl.Create)
			group.POST("/", ctrl.Create)
			group.POST("/list", ctrl.List)
			group.POST("/edit", ctrl.Edit)

			{
				//groupID := group.Use(middleware.IDHandler())

				//groupID.POST("/:id", ctrl.GetByID)
			}

			{
				groupUID := group.Use(middleware.UIDHandler())

				groupUID.POST("/:uid", ctrl.GetByUID)
				groupUID.POST("/:uid/edit", ctrl.Edit)
				groupUID.POST("/:uid/delete", ctrl.Delete)
			}

		}

	}

	return r
}
