package controllers

import (
	"goods-service/repository"
	_ "goods-service/types/base"
	it "goods-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

type ProductController struct {
	Repository repository.ProductRepository
}

//
// @Summary Create new Product
// @Tags Products
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Product Token"
// @Param 	_ body it.InputCreateEditProduct true "Request body"
// @Success 200 {object} Product
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/products/ [post]
func (ctrl *ProductController) Create(c *gin.Context) {
	var input it.InputCreateEditProduct
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64))
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Edit Product
// @Tags Products
// @Description
// @Accept  json
// @Produce  json
// @Param 	_ 	body it.InputCreateEditProduct true "Request body"
// @Param uid path string true "Product UID"
// @Success 200 {object} Product
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/products/{uid}/edit [post]
func (ctrl *ProductController) Edit(c *gin.Context) {
	var input it.InputCreateEditProduct
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Delete product
// @Tags Product
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Product UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/products/{uid}/delete [post]
func (ctrl *ProductController) Delete(c *gin.Context) {

	if err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string)); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: nil})
}

//
// @Summary Get products
// @Tags Products
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListProducts false "Request body"
// @Success 200 {object} []Product
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/products/list [post]
func (ctrl *ProductController) List(c *gin.Context) {

	var input it.InputGetListProduct
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Get one Product
// @Tags Products
// @Description
// @Accept  json
// @Produce  json
// @Param id path string true "Product ID"
// @Success 200 {object} Product
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/products/{id} [post]
func (ctrl *ProductController) GetByID(c *gin.Context) {

	data, err := ctrl.Repository.GetByID(c.MustGet("id").(int64), c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Get one Product
// @Tags Products
// @Description
// @Accept  json
// @Produce  json
// @Param uid path string true "Product UID"
// @Success 200 {object} Product
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/products/{uid} [post]
func (ctrl *ProductController) GetByUID(c *gin.Context) {

	data, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}
