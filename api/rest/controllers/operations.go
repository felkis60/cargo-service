package controllers

import (
	"goods-service/repository"
	it "goods-service/types/input"

	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gin-gonic/gin"
)

type OperationController struct {
	Repository repository.OperationRepository
}

//
// @Summary Create new Operation
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Operation Token"
// @Param 	_ body it.InputCreateEditOperation true "Request body"
// @Success 200 {object} Operation
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/ [post]
func (ctrl *OperationController) Create(c *gin.Context) {
	var input it.InputCreateEditOperation
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64))
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Edit Operation
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param 	_ 	body it.InputCreateEditOperation true "Request body"
// @Param uid path string true "Operation UID"
// @Success 200 {object} Operation
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/{uid}/edit [post]
func (ctrl *OperationController) Edit(c *gin.Context) {
	var input it.InputCreateEditOperation
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Delete operation
// @Tags Operation
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Operation UID"
// @Success 200 {object} Operation
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/{uid}/delete [post]
func (ctrl *OperationController) Delete(c *gin.Context) {

	if err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string)); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: nil})
}

//
// @Summary Get operations
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body it.InputGetListOperations false "Request body"
// @Success 200 {object} []bt.Operation
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/list [post]
func (ctrl *OperationController) List(c *gin.Context) {

	var input it.InputGetListOperation
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Get one Operation
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param id path string true "Operation ID"
// @Success 200 {object} bt.Operation
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/{id} [post]
func (ctrl *OperationController) GetByID(c *gin.Context) {

	data, err := ctrl.Repository.GetByID(c.MustGet("id").(int64), c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Get one Operation
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param uid path string true "Operation UID"
// @Success 200 {object} bt.Operation
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/{uid} [post]
func (ctrl *OperationController) GetByUID(c *gin.Context) {

	data, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, helpers.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, helpers.RestRespone{Message: "Success!", Payload: data})
}
