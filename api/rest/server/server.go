package main

import (
	r "goods-service/api/rest/router"
	s "goods-service/init"
	l "goods-service/log"
	"log"
	"os"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/genproto/googleapis/api/httpbody"
)

func customMatcher(key string) (string, bool) {
	switch key {
	case "Token":
		return key, true
	default:
		return key, false
	}
}

func main() {
	s.SystemStartup(true, true)
	router := r.SetupRouter()
	//router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	log.Printf(l.INFStartServer, "rest")
	router.Run(":" + os.Getenv("REST_PORT"))

	s.SystemStartup(true, true)

	// var grpcServerEndpoint = flag.String("grpc-server-endpoint", ":"+os.Getenv("GRPC_PORT"), "gRPC server endpoint")

	// ctx := context.Background()
	// ctx, cancel := context.WithCancel(ctx)
	// defer cancel()

	// // Register gRPC server endpoint
	// // Note: Make sure the gRPC server is running properly and accessible
	// mux := runtime.NewServeMux(
	// 	runtime.WithIncomingHeaderMatcher(customMatcher),
	// 	runtime.WithMarshalerOption("application/x-ndjson", &HTTPBodyMarshaler{
	// 		Marshaler: &runtime.JSONPb{
	// 			MarshalOptions: protojson.MarshalOptions{
	// 				UseProtoNames:   true,
	// 				EmitUnpopulated: false,
	// 			},
	// 			UnmarshalOptions: protojson.UnmarshalOptions{
	// 				DiscardUnknown: true,
	// 			},
	// 		},
	// 	}),
	// 	runtime.WithMarshalerOption("application/x-ndjson", &HTTPBodyMarshaler{
	// 		Marshaler: &runtime.JSONPb{
	// 			MarshalOptions: protojson.MarshalOptions{
	// 				UseProtoNames:   true,
	// 				EmitUnpopulated: false,
	// 			},
	// 			UnmarshalOptions: protojson.UnmarshalOptions{
	// 				DiscardUnknown: true,
	// 			},
	// 		},
	// 	}),
	// 	runtime.WithErrorHandler(func(ctx context.Context, mux *runtime.ServeMux, marshaler runtime.Marshaler, writer http.ResponseWriter, request *http.Request, err error) {
	// 		//creating a new HTTTPStatusError with a custom status, and passing error
	// 		newError := runtime.HTTPStatusError{
	// 			HTTPStatus: 400,
	// 			Err:        tools.AddServiceNameToError(err),
	// 		}
	// 		// using default handler to do the rest of heavy lifting of marshaling error and adding headers
	// 		runtime.DefaultHTTPErrorHandler(ctx, mux, marshaler, writer, request, &newError)
	// 	}),
	// )
	// opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}

	// if err := proto.RegisterAccountServiceHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts); err != nil {
	// 	log.Print(err)
	// 	return
	// }

	// if err := proto.RegisterOperationServiceHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts); err != nil {
	// 	log.Print(err)
	// 	return
	// }

	// if err := proto.RegisterProductServiceHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts); err != nil {
	// 	log.Print(err)
	// 	return
	// }

	// // Start HTTP server (and proxy calls to gRPC server endpoint)
	// log.Printf(l.INFStartServer, "rest")
	// log.Print(http.ListenAndServe(":"+os.Getenv("REST_PORT"), mux))

}

// HTTPBodyMarshaler is a Marshaler which supports marshaling of a
// google.api.HttpBody message as the full response body if it is
// the actual message used as the response. If not, then this will
// simply fallback to the Marshaler specified as its default Marshaler.
type HTTPBodyMarshaler struct {
	runtime.Marshaler
}

// ContentType returns its specified content type in case v is a
// google.api.HttpBody message, otherwise it will fall back to the default Marshalers
// content type.
func (h *HTTPBodyMarshaler) ContentType(v interface{}) string {
	if httpBody, ok := v.(*httpbody.HttpBody); ok {
		return httpBody.GetContentType()
	}
	return h.Marshaler.ContentType(v)
}

// Marshal marshals "v" by returning the body bytes if v is a
// google.api.HttpBody message, otherwise it falls back to the default Marshaler.
func (h *HTTPBodyMarshaler) Marshal(v interface{}) ([]byte, error) {
	if httpBody, ok := v.(*httpbody.HttpBody); ok {
		return httpBody.Data, nil
	}
	return h.Marshaler.Marshal(v)
}

// no Delimiter for httpbody
func (j *HTTPBodyMarshaler) Delimiter() []byte {
	return []byte{}
}
