package controllers

import (
	"context"
	"encoding/json"

	proto "goods-service/proto"
	"goods-service/repository"
	"goods-service/tools"
	it "goods-service/types/input"
)

type GRPCProductServer struct {
	Repository repository.ProductRepository
	proto.ProductServiceServer
}

func (s *GRPCProductServer) Create(ctx context.Context, req *proto.InputDoByJSON) (*proto.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	var productInput it.InputCreateEditProduct
	if err := json.Unmarshal(req.Body.GetData(), &productInput); err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}
	product, err := s.Repository.Create(&productInput, account.ID)
	//FIX!
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	jsonProduct, err := json.Marshal(product)
	if err != nil {
		return &proto.ServiceResponse{Code: 500, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	return &proto.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonProduct}, nil
}

func (s *GRPCProductServer) Delete(ctx context.Context, req *proto.InputDoByUID) (*proto.ServiceResponse, error) {
	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	err = s.Repository.Delete(req.Uid, req.AccountToken)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	return &proto.ServiceResponse{Code: 200, Message: "Success!", Payload: nil}, nil
}

func (s *GRPCProductServer) GetByID(ctx context.Context, req *proto.InputDoByID) (*proto.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	product, err := s.Repository.GetByID(req.Id, req.AccountToken)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	jsonProduct, err := json.Marshal(product)
	if err != nil {
		return &proto.ServiceResponse{Code: 500, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	return &proto.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonProduct}, nil
}

func (s *GRPCProductServer) GetByUID(ctx context.Context, req *proto.InputDoByUID) (*proto.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	product, err := s.Repository.GetByUID(req.Uid, req.AccountToken)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	jsonProduct, err := json.Marshal(product)
	if err != nil {
		return &proto.ServiceResponse{Code: 500, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	return &proto.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonProduct}, nil
}

func (s *GRPCProductServer) List(ctx context.Context, req *proto.InputDoByJSON) (*proto.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	var productInput it.InputGetListProduct
	if err := json.Unmarshal(req.Body.GetData(), &productInput); err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	data, err := s.Repository.List(req.AccountToken, &productInput)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &proto.ServiceResponse{Code: 500, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	return &proto.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}
