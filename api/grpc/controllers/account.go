package controllers

import (
	proto "goods-service/proto"
	tools "goods-service/tools"

	"context"
	"encoding/json"

	"goods-service/repository"
	bt "goods-service/types/base"
	it "goods-service/types/input"

	"gorm.io/gorm"
)

type GRPCAccountServer struct {
	Repository repository.AccountRepository
	proto.AccountServiceServer
}

func middlewareGetAccount(accountToken string, db *gorm.DB) (*bt.Account, error) {
	var rep = repository.AccountRepository{Db: db}
	return rep.Get(accountToken)

}

func (s *GRPCAccountServer) Create(ctx context.Context, req *proto.InputDoByJSON) (*proto.ServiceResponse, error) {

	var input it.InputCreateEditAccount
	if err := json.Unmarshal(req.Body.GetData(), &input); err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	var account bt.Account
	err := s.Repository.Create(&account, &input)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	jsonAccount, err := json.Marshal(account)
	if err != nil {
		return &proto.ServiceResponse{Code: 500, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	return &proto.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonAccount}, nil
}

func (s *GRPCAccountServer) Edit(ctx context.Context, req *proto.InputEditByJSON) (*proto.ServiceResponse, error) {

	account, err := s.Repository.Get(req.Token)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	var input it.InputCreateEditAccount
	if err := json.Unmarshal(req.Body.GetData(), &input); err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	err = s.Repository.Edit(account, &input, req.Token)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	jsonAccount, err := json.Marshal(account)
	if err != nil {
		return &proto.ServiceResponse{Code: 500, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	return &proto.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonAccount}, nil
}

func (s *GRPCAccountServer) Get(ctx context.Context, req *proto.InputDoByUID) (*proto.ServiceResponse, error) {
	account, err := s.Repository.Get(req.AccountToken)
	if err != nil {
		return &proto.ServiceResponse{Code: 400, Message: err.Error()}, tools.AddServiceNameToError(err)
	}
	jsonAccount, err := json.Marshal(account)
	if err != nil {
		return &proto.ServiceResponse{Code: 500, Message: err.Error()}, tools.AddServiceNameToError(err)
	}

	return &proto.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonAccount}, nil
}
