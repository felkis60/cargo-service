package main

import (
	"log"
	"net"
	"os"

	"goods-service/api/grpc/controllers"
	db "goods-service/database"
	start "goods-service/init"
	logs "goods-service/log"
	pb "goods-service/proto"
	"goods-service/repository"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {

	start.SystemStartup(true, true)

	lis, err := net.Listen("tcp", ":"+os.Getenv("GRPC_PORT"))
	if err != nil {
		log.Fatalf(logs.ERRListen, err)
	}

	s := grpc.NewServer()

	pb.RegisterAccountServiceServer(s, &controllers.GRPCAccountServer{Repository: repository.AccountRepository{Db: db.Db}})
	pb.RegisterProductServiceServer(s, &controllers.GRPCProductServer{Repository: repository.ProductRepository{Db: db.Db}})

	log.Printf(logs.INFStartServer, "grcp")
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf(logs.ERRServe, err)
	}

}
