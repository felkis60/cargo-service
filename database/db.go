package database

import (
	l "goods-service/log"
	bt "goods-service/types/base"
	"log"

	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Db *gorm.DB

func Start() {

	dsn := "host=" + os.Getenv("HOST") +
		" user=" + os.Getenv("DB_USER") +
		" password=" + os.Getenv("DB_PASSWORD") +
		" dbname=" + os.Getenv("DB_NAME") +
		" port=" + os.Getenv("DB_PORT") +
		" sslmode=" + os.Getenv("DB_SSL_MODE") +
		" TimeZone=" + os.Getenv("DB_TIMEZONE")

	var err error
	Db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalf(l.ERRDbConnect, err.Error())
	}
}

func Migrate() {
	err := Db.AutoMigrate(&bt.Account{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.Product{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&bt.Operation{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

}
