package startup

import (
	db "goods-service/database"
	l "goods-service/log"
	"log"

	"github.com/joho/godotenv"
)

func SystemStartup(loadEnv_ bool, logs_ bool) {

	if loadEnv_ {
		if !envInited {
			envInited = true
			err := godotenv.Load("configs/.env")
			if err != nil {
				log.Fatal(l.ERREnvLoad)
			}
		}
	}

	if logs_ {
		if !logsInited {
			logsInited = true
			l.StartLogs()
		}
	}

	if !basicInited {
		basicInited = true
		db.Start()
		db.Migrate()
	}

	if !redis {
		redis = true
		db.InitRedis()
	}

}

var basicInited = false
var envInited = false
var logsInited = false
var redis = false
