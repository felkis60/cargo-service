package tools

import (
	"errors"
)

func AddServiceNameToRedisKey(key *string) {
	*key = "[goods-service]" + *key
}

func AddServiceNameToError(err error) error {
	return errors.New("[goods-service]" + err.Error())
}
