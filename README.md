## Запуск 
    go run ./api/rest/server/.
    go run ./api/grpc/server/.

## Тесты
	go test ./api/rest/test/.

## Swagger
    swag init -o ./api/rest/server/docs

## GRPC Init
    protoc --go_out=. --go_opt=paths=source_relative proto/app.proto
    protoc --go-grpc_out=. --go-grpc_opt=paths=source_relative proto/app.proto
    protoc -I . --grpc-gateway_out=. --grpc-gateway_opt paths=source_relative --grpc-gateway_opt generate_unbound_methods=true --grpc-gateway_opt standalone=true --grpc-gateway_opt logtostderr=true proto/app.proto